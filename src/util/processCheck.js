processCheck = function() {
	if (process.env.PROJECT) {
		return `This is the root for the ${process.env.PROJECT}`;
	} else {
		return "Please set project name in .env using PROJECT = 'Project Name Here' !";
	}
};

module.exports = processCheck;
