
// Packages
const createError = require('http-errors');
const express = require("express");
const bodyParser = require("body-parser")
const logger = require("./config/winston");

// Routes
const indexRouter = require('./routes/index');

// Express and Logging
const app = express();
const morgan = require("morgan");
app.use(morgan("combined", { stream: logger.stream }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : true }));
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
  });

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    logger.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
  
    // render the error page
    res.status(err.status || 500);
    res.send(`Error - ${err.status}`);
  });

module.exports = app;
