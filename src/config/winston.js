const appRoot = require("app-root-path");
const winston = require("winston");

const options = {
    file: {
        level: "info",
        filename: `${appRoot}/logs/app.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false
    },
    console: {
        level: "debug",
        handleExceptions: true,
        json: false,
        colorize: true,
    },
};

const custom = winston.format.printf((info) => {
    return `${info.level}: ${info.message}`;
});

const logger = winston.createLogger({
    transports: [
        new winston.transports.File(options.file),
    ],
    format: winston.format.combine(
        winston.format(info => {
            info.level = `[${info.level.toUpperCase()}]`
            return info;
          })(),
        winston.format.colorize({ all : true }),
        winston.format.simple(),
        winston.format.errors({ stack : true }),
        custom
    ),
    exitOnError: false, // Do not exit on handled exceptions
});

if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console(options.console));
};


logger.stream = {
    write: function(msg, encoding){
        logger.info(msg);
    },
};


module.exports = logger;