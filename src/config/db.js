const DB_USER = process.env.DB_USERNAME;
const DB_PASS = process.env.DB_PASSWORD;
const DB_URI = process.env.DB_URI;
const DB = `mongodb+srv://${DB_USER}:${DB_PASS}@${DB_URI};`

module.exports = DB;