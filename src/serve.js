// Environment
const dotenv = require("dotenv");
dotenv.config();
const PORT = process.env.PORT || 3000;
const DB = require('./config/db');

// DB & App 
const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const app = require('./app');

mongoose.connect(DB, { useNewUrlParser: true })
    .then( async () => {
        try{
            await app.listen(PORT, () => {
                console.log( `App is listening at on port ${PORT}...
                \nPress CTRL-C to stop.`);
            });
        } catch (e){
            console.error(e);
        }
    });

mongoose.connection.once("open", () => {
    console.log('Connected to Database...')
});

module.exports = app;