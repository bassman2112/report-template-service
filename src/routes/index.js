const express = require('express');
const router = express.Router();
const processCheck = require('../util/processCheck');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.status(200).send(processCheck());
});

module.exports = router;