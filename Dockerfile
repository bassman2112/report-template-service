
#
# Will split this into two steps
#

FROM node:10-alpine

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY .npmrc /usr/src/app/

COPY package.json /usr/src/app/
RUN npm install --quiet

COPY . /usr/src/app

EXPOSE $PORT
CMD [ "npm", "run", "start" ]
